const fs = require('fs')
const readline = require('readline')
const { exec } = require('child_process')

const file = process.argv[2]

if (!file) {
	console.error('Es necesario indicar un fichero de entrada')
	return
}

const readInterface = readline.createInterface({
    input: fs.createReadStream(file),
    output: process.stdout,
    terminal: false
})

readInterface.on('line', (file) => {
	const fileName = `echo ${file}`
	const separator = `echo ";"`
	const hashFile = `md5sum ${file} | awk '{ print $1 }'`
	const hashFileProperties = `ls -l ${file} | md5sum | awk '{ print $1 }'`
	const command = `{ ${fileName} && ${separator} && ${hashFile} && ${separator} && ${hashFileProperties} ;} | tr "\n" " "`
	
    fs.writeFile('output.txt', '', () => {})
	exec(command, (error, stdout, stderr) => {
    	if (error) {
        	console.log(`error: ${error.message}`)
        	return
    	}
    	if (stderr) {
        	console.log(`stderr: ${stderr}`)
        	return
    	}
    	
		fs.appendFile('output.txt', stdout + "\n", (err) => {
  			if (err) {
        		console.log(`error: ${error.message}`)
        		return
  			}
		})
    	console.log(`output.txt << ${stdout}`)
	})
})

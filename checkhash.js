const fs = require('fs')
const readline = require('readline')
const { exec } = require('child_process')

const file = process.argv[2]

if (!file) {
	console.error('Es necesario indicar un fichero de entrada')
	return
}

const readInterface = readline.createInterface({
    input: fs.createReadStream(file),
    output: process.stdout,
    terminal: false
})

readInterface.on('line', (file) => {
	const [fileName] = file.split(';')
	
	const fileNameEcho = `echo ${fileName.trim()}`
	const separatorEcho = `echo ";"`
	const hashFile = `md5sum ${fileName} | awk '{ print $1 }'`
	const hashFileProperties = `ls -l ${fileName} | md5sum  | awk '{ print $1 }'`
	const command = `{ ${fileNameEcho} && ${separatorEcho} && ${hashFile} && ${separatorEcho} && ${hashFileProperties} ;} | tr "\n" " "`
	
	exec(command, (error, stdout, stderr) => {
    	if (error) {
        	console.log(`error: ${error.message}`)
        	return
    	}
    	if (stderr) {
        	console.log(`stderr: ${stderr}`)
        	return
    	}
    	
		const [, lastHashFile, lastHashFileProperties] = file.split(';')
		const [fileName, hashFile, hashFileProperties] = stdout.split(';')
    	
    	if (lastHashFile === hashFile && lastHashFileProperties === hashFileProperties) {
    		console.log(`File: ${fileName}, has not been modified`)
    	} else if (lastHashFile !== hashFile) {
    		console.log(`File: ${fileName}, content of file has been modified`)
    	} else {
    		console.log(`File: ${fileName}, has been modified`)
    	}
	})
})
